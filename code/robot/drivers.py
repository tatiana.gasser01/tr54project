#!/usr/bin/env pybricks-micropython
from pybricks.ev3devices import Motor, TouchSensor, InfraredSensor, UltrasonicSensor, ColorSensor

from pybricks.tools import StopWatch, DataLog, wait
from pybricks.parameters import Color, Port
from pybricks.robotics import DriveBase


class MotionDriver:
    """
    This class controls motors to provide basic motion abilities
    """

    def __init__(self, left_motor_port=Port.B,
                 right_motor_port=Port.C,
                 wheel_diameter=56, axle_track=134,
                 min_steering=-100, max_steering=100,
                 min_speed=0, max_speed=100):
        """
        Initializes a controller with two wheels.
        :param left_motor_port: the output pin for the left motor
        :param right_motor_port: the output pin for the right motor
        :param wheel_diameter: the diameter of the wheel in mm
        :param axle_track: the axle track length in mm
        :param min_steering: the minimum steering
        :param max_steering: the maximum steering
        :param min_speed: the minimum speed
        :param max_speed: the maximum speed
        """
        self.right_motor = Motor(left_motor_port)
        self.left_motor = Motor(right_motor_port)

        self.min_steering = min_steering
        self.max_steering = max_steering
        self.min_speed = min_speed
        self.max_speed = max_speed

    def drive(self, steering, speed):
        """
        Drives using the given steering and speed limited to defined bounds.
        :param steering: the steering, will be truncated to fit in [-100; 100]
        :param speed: the speed, will be truncated to fit in [-100; 100]
        """
        steering = -steering
        apply_steering = min(self.max_steering, max(self.min_steering, steering))
        apply_speed = min(self.max_speed, max(self.min_speed, speed))

        left_dc = min(apply_speed, apply_speed + apply_steering * (apply_speed / 50))
        right_dc = min(apply_speed, apply_speed - apply_steering * (apply_speed / 50))

        if right_dc < 5:
            right_dc = 0
        
        if left_dc < 5:
            left_dc = 0

        self.right_motor.dc(right_dc)
        self.left_motor.dc(left_dc)

        return self.left_motor.speed(), self.right_motor.speed()


class MetricSensor:
    """
    Enumeration used to define the metric sensor type of the robot
    """
    INFRARED = 1
    ULTRASONIC = 2


class MetricsSensorComponent:
    """
    This class controls a metric sensor to get distance values
    """

    # Defines the factor between infrared sensor output and the value in mm
    SENSOR_CALIBRATION = 142 / 42   # Dummy value since we use ultrasonic sensor

    def __init__(self, sensor_type, port):
        """
        Initiates a MetricSensorComponent

        :param sensor_type: the type of the sensor must be a value of `MetricSensor`
        :param port: the sensor's EV3 port
        """
        self.type = sensor_type

        if sensor_type == MetricSensor.INFRARED:
            self.sensor = InfraredSensor(port)
        elif sensor_type == MetricSensor.ULTRASONIC:
            self.sensor = UltrasonicSensor(port)
        else:
            raise Exception("Unsupported sensor type")

    def distance(self):
        """
        Returns the distance between the sensor and the next obstacle in mm
        """
        if self.type == MetricSensor.INFRARED:
            return self.sensor.distance() * self.SENSOR_CALIBRATION
        elif self.type == MetricSensor.ULTRASONIC:
            return self.sensor.distance(silent=False)
        else:
            raise Exception("Unsupported sensor type")


class ColorSensorComponent:
    """
    This class controls the color sensor to give the read RGB color or the color intensity
    """

    def __init__(self, port):
        """
        Initiates a ColorSensorComponent

        :param port: the EV3 port of the color sensor
        """
        self.sensor = ColorSensor(port)

    def read_color(self):
        """
        Returns a tuple (R, G, B) with each going from 0 to 100
        """
        return self.sensor.rgb()

    def read_intensity(self):
        """
        Returns the reflected light intensity from 0 to 100
        """
        return self.sensor.reflection()


class TouchSensorComponent:
    """
    This class interfaces a touch sensor, e.g. a button
    """

    def __init__(self, port):
        """
        Initiates a TouchSensorComponent

        :param port: the EV3 port of the touch sensor
        """
        self.sensor = TouchSensor(port)

    def touch(self):
        """
        Returns True if the button is pressed, False otherwise
        """
        return self.sensor.pressed()


class LCDSensorComponent:
    """
    This class controls the embedded LCD screen of EV3 robots
    """

    def __init__(self, screen):
        """
        Initiates a LCDSensorComponent

        :param screen: an instance of `EV3Brick.screen`
        """
        self.screen = screen

    def clear_and_print(self, msg):
        """
        Clears the LCD screen and print the given message

        :param msg: the message to print
        """
        self.screen.clear()
        self.screen.print(msg)


class SoundAndLightsComponent:
    """
    This class controls the speakers and lights of the EV3 brick
    """

    def __init__(self, speaker, light):
        """
        Instantiates a SoundAndLightsComponent

        :param speaker: an instance of `EV3Brick.speaker`, can be None since not used anymore
        :param light: an instance of `EV3Brick.light`
        """
        self.speaker = speaker
        self.light = light

    def set_light_color(self, color):
        """
        Sets the displayed color

        :param color: the color to be displayed
        """
        self.light.on(color)
