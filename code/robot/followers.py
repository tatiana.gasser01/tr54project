

class AbstractFollower:
    """
    This class represent an AbstractFollower, each follower implementation must inherit and implement
    methods of this class
    """

    def __init__(self):
        """
        Instantiates an AbstractFollower
        """
        self.distance = 0
        self.speed = 0

    def follow_tick(self, delta_time, distance):
        """
        Computes the speed to be applied in order to follow the robot next to this one without any collision

        :param delta_time: in seconds, gives the elapsed time between now and the last call to this method
        :param distance: gives the distance between this robot and the next one, should be given in mm
        """
        raise Exception("Unimplemented method")

    def get_last_distance(self):
        """
        Returns the last retained distance
        """
        return self.distance

    def get_last_speed(self):
        """
        Returns the last computed speed
        """
        return self.speed


class TwoPoint(AbstractFollower):
    """
        This class represents a follower using two points, or two zones which defines breaking and accelerating areas
    """

    def __init__(self, aa, af, da, df, max_a, max_f, max_speed=50):
        """
        Initiates an instance of TwoPoint follower

        :param aa: defines the acceleration coefficient to apply
        :param af: defines the breaking coefficient to apply
        :param da: defines the minimum distance to be reached before going forward (positive acceleration)
        :param df: defines the maximum distance to be reached before breaking
        :param max_a: defines the maximal acceleration
        :param max_f: defines the maximal break force
        :param max_speed: defines the maximum speed to apply
        """
        super().__init__()
        self.aa = aa
        self.af = af
        self.da = da
        self.df = df
        self.max_a = max_a
        self.max_f = max_f
        self.max_speed = max_speed

    def follow_tick(self, delta_time, d):
        """
        Computes the speed to be applied in order to follow the robot next to this one without any collision

        :param delta_time: in seconds, gives the elapsed time between now and the last call to this method
        :param d: gives the distance between this robot and the next one, should be given in mm
        """

        brake_speed = min(max(self.af * (d - self.df), 0), self.max_speed)
        accel_speed = min(max(self.aa * (d - self.da), 0, self.get_last_speed()), self.max_speed)

        if delta_time > 0:
            accel_speed = min(accel_speed, self.max_a * delta_time + self.speed)
            brake_speed = max(brake_speed, -self.max_f * delta_time + self.speed)

        s = min(brake_speed, accel_speed)

        self.speed = s
        self.distance = d

        return self.speed
