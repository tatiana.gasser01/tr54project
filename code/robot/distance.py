class DistanceEstimtor:
    """
    This class estimates traveled distance from instantiation or last manual reset
    """

    def __init__(self, factor):
        """
        Creates a direction estimator component

        :param factor: defines the ratio between input speed given to the `estimate` function
                       this ratio should transform input speed to speed in cm/s
        """
        self._dist = 0
        self._factor = factor
    
    def reset(self):
        """
        Resets the cumulated distance
        """
        self._dist = 0

    def get_distance(self):
        """
        Returns the cumulated distance
        """
        return self._dist

    def estimate(self, v1, v2, delta_time):
        """
        Updates the cumulated distance

        :param v1: the speed of left motor
        :param v2: the speed of right motor
        :param delta_time: the elapsed time between now and last call to this function
        """

        mean = (v1 + v2) / 2
        dist = mean * delta_time * self._factor
        self._dist += dist
        return dist
