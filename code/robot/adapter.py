

def _map(a, a0, a1, b0, b1):
    """
    Internal utility function, maps a value `a` in range [a0, a1] to a value in range [b0, b1]

    :param a: the value to map
    :param a0: lower bound of the range of `a`
    :param a1: upper bound of the range of `a`
    :param b0: lower bound of the new range
    :param b1: upper bound of the new range

    :return the value in the b range
    """
    return b0 + (b1 - b0) * ((a - a0) / (a1 - a0))


class ColorAdapter:
    """
    Utility class used to adapt the given color intensity into the range [0, 100] with grey at 50
    """

    def __init__(self, white, grey, black):
        """
        Creates an instance of ColorAdapter according to given calibration values

        :param white: the color intensity for the white color
        :param grey: the color intensity for the grey color
        :param black: the color intensity for the black color
        """
        self.white = white
        self.grey = grey
        self.black = black

    def transform(self, intensity):
        """
        Re-maps the given color intensity to a true [0, 100]  range

        :param intensity: the read color intensity taken from the sensor

        :return the mapped value
        """
        if intensity < self.grey:
            return max(0, _map(intensity, self.black, self.grey, 0, 50))
        
        return min(_map(intensity, self.grey, self.white, 50, 100), 100)
