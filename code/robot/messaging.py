from umqtt.robust import *
from ucollections import deque
from topics import Topic
import time


class Message:
    """
    This class holds data for a MQTT message, e.g. topic and payload
    """

    def __init__(self, topic, payload):
        """
        Creates a Message

        :param topic: the MQTT topic
        :param payload: the MQTT payload
        """
        self.topic = topic
        self.payload = payload

    def same_topic(self, topic):
        """
        Checks if the given topic is the same of this

        :param topic: the topic to be compared with this.topic
        """
        return self.topic == topic.encode()


class MessengerClient:
    """
    Provides a messaging client based on top of MQTT
    """

    def __init__(self, client_id: str, host: str, topics):
        """
        Creates a new instance of MessengerClient

        :param client_id: the MQTT client id
        :param host: the MQTT broker IP
        :param topics: the topics to subscribe to
        """

        self.client = MQTTClient(client_id, host)
        self.topics = topics
        self.queue_received = deque((), 64)
        self.queue_send = deque((), 64)
        self.lwt = None
        self.lwt_msg = None
        self.run = 1
        
    def _on_message(self, topic, payload):
        """
        Internal method
        Intercepts mqtt message and stores it into a queue
        """
        msg = Message(topic, payload)
        self.queue_received.append(msg)

    def subscribe(self, topic: Topic):
        """
        Subscribes to a given topic

        :param topic: the topic to subscribe to
        """
        self.client.subscribe(topic.topic, qos=topic.qos)

    def set_last_will(self, lwt_topic, lwt_message):
        """
        Sets the Last Will Testament message

        :param lwt_topic: the LWT topic
        :param lwt_message: the LWT message
        """
        self.lwt = lwt_topic
        self.lwt_msg = lwt_message
        self.client.set_last_will(lwt_topic.topic, lwt_message, qos=lwt_topic.qos)

    def connect(self):
        """
        Connects to the MQTT broker
        """
        self.client.connect()

        self.client.set_callback(self._on_message)

        for topic in self.topics:
            self.subscribe(topic)

    def reconnect(self):
        """
        Re-connects to the MQTT broker
        """
        self.client.reconnect()

    def disconnect(self):
        """
        Disconnects from the MQTT broker
        """
        self.client.disconnect()

    def disconnect_lwt(self):
        """
        Disconnects from the MQTT broker and send the Last Will And Testament message if defined
        """
        if self.lwt is not None:
            self._send_msg(self.lwt, self.lwt_msg)
            time.sleep(1)
        self.disconnect()

    def _send_msg(self, topic: Topic, payload):
        """
        Internal methods to publish a MQTT message,
        Not thread safe
        """
        self.client.publish(topic.topic, str(payload), qos=topic.qos)

    def send_msg(self, topic: Topic, payload):
        """
        Thread safe method to send a MQTT message (publish)
        The MQTT thread must be started

        :param topic: the message topic
        :param payload: the message payload
        """
        msg = Message(topic, payload)
        self.queue_send.append(msg)

    def send_sync(self, topic: Topic, payload):
        """
        Not thread safe method to send a MQTT message (publish)
        The MQTT thread must be stopped

        :param topic: the message topic
        :param payload: the message payload
        """
        self.client.publish(topic.topic, str(payload), qos=topic.qos)

    def get_msg(self):
        """
        Thread safe method to get the last recorded MQTT message

        :return a Message instance if any available, None otherwise
        """
        if len(self.queue_received) == 0:
            return None

        return self.queue_received.popleft()

    def sync_wait_for_topic(self, topic: str):
        """
        Synchronously wait for a given topic, all other received messages will be dropped forever
        The MQTT thread must be stopped

        :param topic: the topic to wait for
        """
        while True:
            while len(self.queue_received) == 0:
                self.client.check_msg()
                time.sleep(0.005)

            item = self.queue_received.popleft()
            if item.same_topic(topic):
                break

        return item

    def wait_for_topic(self, topic: str):
        """
        Wait for a given topic, all other received messages will be dropped forever
        The calling thread is passively blocked
        The MQTT thread must be started

        :param topic: the topic to wait for
        """
        while True:
            while len(self.queue_received) == 0:
                time.sleep(0.005)

            item = self.queue_received.popleft()
            if item.same_topic(topic):
                break

        return item

    def thread_mqtt(self):
        """
        Defines the MQTT thread loop
        To stop, you may first need to set `self.run` to 0
        """
        while self.run != 0:
            # Queue received msg
            try:
                self.client.check_msg()
            except:
                print('Got mqtt exception')

            # Queue send msg
            if len(self.queue_send) != 0:
                msg = self.queue_send.popleft()
                self._send_msg(msg.topic, msg.payload)
