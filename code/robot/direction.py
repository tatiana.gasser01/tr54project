from ucollections import deque
from math import *


class Direction:
    """
    An enumeration representing the direction of the robot
    """

    __dirs = {
        "-1": "unknown",
        "1": "straight",
        "2": "turning right",
        "3": "turning left",
    }

    UNKNOWN = -1
    STRAIGHT = 1
    TURNING_RIGHT = 2
    TURNING_LEFT = 3

    @staticmethod
    def to_str(direction: int):
        """
        Utility function to get a string representation of the given direction
        """
        return Direction.__dirs.get(str(direction), "unknown")


class DirectionEstimator:
    """
    This class defines a component able to estimate the direction of a robot according to its cumulated steering
    """

    def __init__(self, lookup_period_ms=2_000, nominal_delta_time_ms=100, threshold=50):
        """
        Instantiates an DirectionEstimator component

        :param lookup_period_ms: defines the life time in ms of a recorded value. e.g. after lookup_period_ms is reached
                                 the value is forgotten
        :param nominal_delta_time_ms: defines the expected delta time between two call to `estimate`
        :param threshold: this parameter defines the minimum value to be reached before considering that the robot is
                          not going straight
        """
        self._lookup_period_ms = lookup_period_ms
        self._cumulated_time = 0
        self._cumulated_steering = 0
        self._threshold = threshold
        self._nominal_delta_time_ms = nominal_delta_time_ms
        self._max_nb_val = ceil(1.05 * lookup_period_ms / nominal_delta_time_ms)
        self._steering_values = deque((), self._max_nb_val)

    def estimate(self, steering, delta_time):
        """
        Estimates the robot direction according to cumulated steering over the lookup period
        """
        self._cumulated_time += delta_time
        self._cumulated_steering += steering

        self._steering_values.append(steering)
        nb_item_to_pop = max(0,
                             floor((self._cumulated_time - self._lookup_period_ms) / self._nominal_delta_time_ms))
        self._cumulated_time -= nb_item_to_pop * self._nominal_delta_time_ms

        if nb_item_to_pop > 0:
            self._cumulated_steering -= self._steering_values.popleft()

        if self._cumulated_steering < -self._threshold:
            return Direction.TURNING_LEFT

        if self._cumulated_steering > self._threshold:
            return Direction.TURNING_RIGHT

        return Direction.STRAIGHT
