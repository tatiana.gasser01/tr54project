class SteeringComponent:
    """ Defines an abstract component able to compute a steering value method. """

    def __init__(self, instruction=50):
        """
        Initializes the steering component.
        :param instruction: the order to achieve
        """
        self._instruction = instruction

    def compute_error(self, color_intensity):
        """
        Computes instantaneous error
        :param color_intensity: measured color intensity at this time
        :return the instantaneous error
        """
        return color_intensity - self._instruction

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this descision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """
        raise Exception("Not implemented")


class BangBangSteeringComponent(SteeringComponent):
    """ Defines a component able to compute a steering value from Bang-Bang method. """

    def __init__(self, alpha, speed, instruction=50):
        """
        Initializes the steering component.
        :param alpha: the steering to apply when instruction not reached
        :param speed: the speed to apply
        :param instruction: the order to achieve
        """
        super().__init__(instruction)
        self._alpha = alpha
        self._speed = speed

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this descision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """
        error = self.compute_error(color_intensity)
        alpha = 0
        if error < 0:
            alpha = -self._alpha
        elif error > 0:
            alpha = self._alpha

        return alpha, self._speed, error


class ProportionalSteeringComponent(SteeringComponent):
    """ Defines a component able to compute a steering value from P method. """

    def __init__(self, k_p, speed, instruction=50):
        """
        Initializes the steering component.
        :param k_p: proportional coefficient
        :param speed: the speed to apply
        :param instruction: the order to achieve
        """
        super().__init__(instruction)
        self._kp = k_p
        self._speed = speed

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this descision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """
        error = self.compute_error(color_intensity)
        return self._kp * error, self._speed, error


class PISteeringComponent(SteeringComponent):
    """ Defines a component able to compute a steering value from PI method. """

    def __init__(self, k_p, k_i, speed, instruction=50, d_bar=50):
        """
        Initializes the steering component.
        :param k_p: proportional coefficient
        :param k_i: the integral coefficient
        :param speed: the speed to apply
        :param instruction: the order to achieve
        :param d_bar: defines command boundaries e.g. integral bounded in [-d_bar, d_bar]
        """
        super().__init__(instruction)
        # re-use the proportional component
        self._p_steering = ProportionalSteeringComponent(k_p, speed, instruction)
        self._k_i = k_i
        self._speed = speed
        self._error_sum = 0
        self._d_bar = d_bar

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this descision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """
        p, _, error = self._p_steering.compute(color_intensity, delta_time)
        self._error_sum += error
        i = self._k_i * delta_time * self._error_sum

        # Bound integral error to [-d_bar, d_bar]
        i = min(self._d_bar, max(-self._d_bar, i))
        return p + i, self._speed, error


class PIDSteeringComponent(SteeringComponent):
    """ Defines a component able to compute a steering value from PID method. """

    def __init__(self, k_p, k_i, k_d, speed, instruction=50, d_bar=50):
        """
        Initializes the steering component.
        :param k_p: proportional coefficient
        :param k_i: the integral coefficient
        :param k_d: the derivative coefficient
        :param speed: the speed to apply
        :param instruction: the order to achieve
        :param d_bar: defines command boundaries e.g. integral bounded in [-d_bar, d_bar]
        """
        super().__init__(instruction)
        self._pi_steering = PISteeringComponent(k_p, k_i, speed, instruction, d_bar)
        self._k_d = k_d
        self._speed = speed
        self._last_error = 0

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this descision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """

        pi, _, error = self._pi_steering.compute(color_intensity, delta_time)

        # In case of a zero or nearly zero only use PI
        if delta_time <= 0.00001:
            self._last_error = error
            return pi, self._speed, error

        d = (self._k_d / delta_time) * (error - self._last_error)
        self._last_error = error
        return pi + d, self._speed, error


class PIDAdvancedSteeringComponent(SteeringComponent):
    """ Defines a component able to compute a steering value from PID method with adaptive k_i and speed. """

    def __init__(self, k_p, k_i, k_i_2, k_d, min_at_err, min_speed, max_speed, instruction=50, d_bar=50):
        """
        Initializes the steering component.
        :param k_p: proportional coefficient
        :param k_i: the integral coefficient applied in positive error
        :param k_i_2: the integral coefficient applied in negative error
        :param k_d: the derivative coefficient
        :param min_at_err: choose min_speed when |instantaneous error| >= min_at_err
        :param min_speed: the minimum speed to apply when error is high
        :param max_speed: the maximum speed to apply when error is low
        :param instruction: the order to achieve
        :param d_bar: defines command boundaries e.g. integral bounded in [-d_bar, d_bar]
        """
        super().__init__(instruction)
        self._pid_steering = PIDSteeringComponent(k_p, k_i, k_d, min_speed, instruction, d_bar)
        self._p_s = (min_speed - max_speed) / min_at_err
        self._min_speed = min_speed
        self._max_speed = max_speed
        self._k_i = k_i
        self._k_i2 = k_i_2

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this decision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """
        error = self.compute_error(color_intensity)
        # Select ki according to error sign
        if error < 0:
            self._pid_steering._pi_steering._k_i = self._k_i2
        else:
            self._pid_steering._pi_steering._k_i = self._k_i

        # Get the command by PID computing
        pid, s, err = self._pid_steering.compute(color_intensity, delta_time)

        # Compute the proportional speed
        speed = max(self._max_speed + abs(pid) * self._p_s, self._min_speed)
        # print(err, speed)

        return pid, speed, err


class SimplePIDSteeringComponent(SteeringComponent):
    """ Defines a basic PID component. """

    def __init__(self, kp, ki, ki_2, kd, speed, instruction):
        """
        Initializes the PID parameters.
        :param kp: the proportional coefficient
        :param ki: the integral coefficient
        :param kd: the derivative coefficient
        :param instruction: the order to achieve
        :param speed: the desired speed
        """
        super().__init__(instruction)

        self._kp = kp
        self._ki = ki
        self._ki_2 = ki_2
        self._kd = kd
        self._speed = speed
        self._error = 0
        self._sum_error = 0
        self._delta_error = 0
        self._last_error = 0

    def compute(self, color_intensity, delta_time):
        """
        Computes the steering and speed to apply to reach the instruction
        :param color_intensity: measured color intensity at this time
        :param delta_time: the delta_time between this decision and the previous one
        :return (steering command(float), speed command (float), error (float) )
        """
        error = self.compute_error(color_intensity)
        self._sum_error += self._error
        self._delta_error = self._error - self._last_error
        self._last_error = self._error
        p = self._kp * error
        if error < 0:
            i = self._ki * self._sum_error * delta_time
        else:
            i = self._ki_2 * self._sum_error * delta_time

        d = self._kd * (self._delta_error / delta_time)
        return p + i + d, self._speed, error
