from direction import *
from position import *
from road import *


class Robot:
    """ Defines a robot with a controller component, a steering component and a color sensor. """

    NO_STOP = 9999999  # Does not stop programmatically the robot

    def __init__(self, controller_component, follower_component,
                 steering_component, color_sensor_component, distance_component, lcd_component,
                 direction_estimator, position_estimator, distance_estimator, color_adapter, initial_dist=0, 
                 secure_dist=10):
        """
        Initializes the robot with the given controller and components.
        :param controller_component: the controller component that controls wheel motors
        :param follower_component: the follower component to avoid collisions and smooth following
        :param steering_component: the steering component able to compute the desired steering
        :param color_sensor_component: the color sensor component able to read the floor color
        :param distance_component: the component able to read distance using ultrasonic or infrared sensor
        :param lcd_component: the component able to control the LCD screen
        :param direction_estimator: a component able to estimate the robot direction
        :param color_adapter: a component able to re-map color range
        """
        self._controller_component = controller_component
        self._follower_component = follower_component
        self._steering_component = steering_component
        self._color_sensor_component = color_sensor_component
        self._distance_component = distance_component
        self._lcd_component = lcd_component
        self._direction_estimator = direction_estimator
        self._position_estimator = position_estimator
        self._distance_estimator = distance_estimator
        self._steering = 0
        self._cumulated_time = 0
        self._old_right_speed = 0
        self._old_left_speed = 0
        self._right_speed = 0
        self._left_speed = 0
        self._dist = initial_dist
        self._direction = Direction.UNKNOWN
        self._position = Position.UNKNOWN
        self._color_adapter = color_adapter
        self._last_recalibrate_dist = 0
        self._road_position = RoadPosition(get_road(RoadEnum.UNKNOWN), initial_dist)
        self._force_stop = self.NO_STOP
        self._cumulate_distance_from_beg = initial_dist
        self._secure_dist = secure_dist
        self._min_recalibrate_dist = min(get_road(RoadEnum.TOP_LEFT)._length, get_road(RoadEnum.BOTTOM_LEFT)._length) - 20
        self.max_recalibrate_dist = self._min_recalibrate_dist + 80

    def drive(self, delta_time, force=False, recalibrate=False):
        """
        Drives the robot according to the given delta time.
        :param delta_time: the delta time in seconds.
        """
        self._cumulated_time += delta_time
        color_intensity = self._color_sensor_component.read_intensity()
        color_intensity = self._color_adapter.transform(color_intensity)
        self._old_left_speed = self._left_speed
        self._old_right_speed = self._right_speed
        d = self._distance_component.distance()
        d = min(d, self._force_stop)
        self._steering, speed, _ = self._steering_component.compute(color_intensity, delta_time)
        speed_follow = self._follower_component.follow_tick(delta_time, d)
        (self._left_speed, self._right_speed) = self._controller_component.drive(self._steering, min(speed, speed_follow))
        self.estimate_direction(delta_time * 1000)
        return self.estimate_position(delta_time * 1000, force, recalibrate)

    def start_recalibrate(self):
        self._last_recalibrate_dist = self._distance_estimator.get_distance() - self.get_position().traveled_dist

    def estimate_direction(self, delta_time):
        self._direction = self._direction_estimator.estimate(self._steering, delta_time)

    def estimate_position(self, delta_time, force=False, recalibrate=False):
        self._dist = self._distance_estimator.estimate(self._old_left_speed, self._old_right_speed, delta_time / 1000)
        t_d = self._distance_estimator.get_distance()
        self._position = self._position_estimator.estimate(self._direction, delta_time)
        self._road_position.compute_new_position(self._dist)

        if self._road_position.road.is_critical() and not force:
            self._force_stop = (self._road_position.remaining_dist() - self._secure_dist) * 10
        else:
            self._force_stop = self.NO_STOP


        if recalibrate and t_d - self._last_recalibrate_dist > self._min_recalibrate_dist:
            if self._direction == Direction.TURNING_LEFT:
                self._road_position = RoadPosition(get_road(RoadEnum.BOTTOM), 0)
                print("Reset to bot")
                return False
            elif self._direction == Direction.TURNING_RIGHT:
                self._road_position = RoadPosition(get_road(RoadEnum.TOP), 0)
                print("Reset to top")
                return False
        
        if recalibrate and t_d - self._last_recalibrate_dist > self.max_recalibrate_dist:
            return False

        return recalibrate

    def get_position(self):
        return self._road_position

    def get_steering(self):
        """
        Gets the current steering.
        :return: the current steering.
        """
        return self._steering
