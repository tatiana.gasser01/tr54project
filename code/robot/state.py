class State:
    """
    Enumeration to represent a robot state
    """
    ROAD_UNKNOWN = 0
    DRIVING = 1
    ENTER_CRITICAL = 2
    ENTER_CRITICAL_ACK = 3
    ALLOWED = 4
    EXITING = 5
