#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.parameters import Port, Color
from position import *
import random
from drivers import *
from followers import TwoPoint
from messaging import MessengerClient
from road import *
from robot import Robot
from steering import *
from direction import *
from distance import *
import time
from time import sleep
import _thread
from adapter import *
from topics import *
from state import *

# Const

# MQTT client id
MESSENGER_ID = str(random.randint(0, 10000))

# MQTT broker IP
# MESSENGER_HOST = '192.168.1.101'
MESSENGER_HOST = '192.168.43.3'

# MQTT topics to subscribe to
MESSENGER_TOPICS = (ENTER_ACK_TOPIC, EXIT_ACK_TOPIC, ALLOWED_TOPIC)

# Length of the critical zone in cm
CRITICAL_ZONE_SIZE = 300

# Minimum waiting time in seconds between two iterations of the main loop
MIN_DELTA_TIME = 0.1

# Minimum time in seconds before sending position to the server
MIN_POSITION_LOG_TIME = 0.25

# Safe distance in centimeter to be applied when crossing the cross road before sending the exit topic
CROSS_DST = 40

# Format the robot id topic according to the MQTT ID,
# the later is used to get back the available robot ids from the server
ROBOT_ID_TOPIC.format(MESSENGER_ID)


# Shared between threads
messenger = MessengerClient(MESSENGER_ID, MESSENGER_HOST, [ROBOT_ID_TOPIC])  # MQTT client


def main():
    """
    The main thread, contains main logic such as MQTT initialisation, line following, other robot following
    critical zone management
    """

    # Initialize the EV3 Brick.
    ev3 = EV3Brick()

    # Create all basic components
    motion_driver = MotionDriver(Port.B, Port.C)
    touch = TouchSensorComponent(Port.S4)
    color_sensor_component = ColorSensorComponent(Port.S3)
    lcd_component = LCDSensorComponent(ev3.screen)
    distance_component = MetricsSensorComponent(MetricSensor.ULTRASONIC, Port.S2)
    lights_component = SoundAndLightsComponent(ev3.speaker, ev3.light)
    follower_component = TwoPoint(aa=1.5, af=1, da=170, df=120, max_a=20, max_f=70, max_speed=50)
    steering_component = SimplePIDSteeringComponent(kp=0.5, ki=0.9, ki_2=0.9, kd=0.1, speed=50, instruction=50)

    direction_estimator = DirectionEstimator(lookup_period_ms=2000, threshold=175)
    position_estimator = PositionEstimator()
    distance_estimator = DistanceEstimtor(0.047997)

    # Set boot/config color
    lights_component.set_light_color(Color.ORANGE)

    # Color sensor calibration
    def get_color_input(txt):
        v = 0
        while not touch.touch():
            lcd_component.clear_and_print(txt)
            v = color_sensor_component.read_intensity()
            sleep(0.2)

        sleep(1)
        return v

    color_tab = (get_color_input('White'),
                 get_color_input('Grey'),
                 get_color_input('Black'))

    # Create adapter to map color sensor values to [0, 100]
    color_adapter = ColorAdapter(*color_tab)

    # Create the robot instance
    robot = Robot(motion_driver, follower_component, steering_component, color_sensor_component,
                  distance_component, lcd_component, direction_estimator, position_estimator,
                  distance_estimator, color_adapter, initial_dist=23.5, secure_dist=10)

    # Request a robot id to the server
    lcd_component.clear_and_print('Getting id...')
    messenger.send_sync(REQUEST_ROBOT_ID_TOPIC, MESSENGER_ID)
    msg = messenger.sync_wait_for_topic(ROBOT_ID_TOPIC.topic)
    robot_id = int(msg.payload)
    print('Got id', robot_id)

    # Reconnect to MQTT broker to set our LWT. The later releases the robot id so that the server knows
    # that the id is no longer used
    lcd_component.clear_and_print('Reconnecting...')
    messenger.disconnect()
    sleep(0.5)
    messenger.reconnect()

    # Subscribes to topics
    for topic in MESSENGER_TOPICS:
        topic.format(robot_id)
        messenger.subscribe(topic)

    # Start MQTT thread
    _thread.start_new_thread(messenger.thread_mqtt, ())

    lcd_component.clear_and_print('Driving...')

    start = time.time()
    current_time = start
    no_position_log_since = 0
    position = robot.get_position()
    next_road = 0
    should_recalibrate = True

    state = State.ROAD_UNKNOWN

    # Main driving loop
    while not touch.touch():

        # Update state according to the knowledge of the road
        if position.road.get_num() == RoadEnum.UNKNOWN:
            state = State.ROAD_UNKNOWN
        elif state == State.ROAD_UNKNOWN:
            state = State.DRIVING

        # Display the right color according to the robot sate
        if state == State.ROAD_UNKNOWN:
            # Set unknown road color
            lights_component.set_light_color(Color.RED)
        elif state == State.DRIVING:
            # Set running color
            lights_component.set_light_color(Color.YELLOW)
        elif state == State.ENTER_CRITICAL or state == State.ENTER_CRITICAL_ACK:
            lights_component.set_light_color(Color.RED)
        else:
            lights_component.set_light_color(Color.GREEN)

        # Check for incoming MQTT messages
        msg = messenger.get_msg()
        if msg is not None:
            if msg.same_topic(ENTER_ACK_TOPIC.topic):
                state = State.ENTER_CRITICAL_ACK
            elif msg.same_topic(ALLOWED_TOPIC.topic):
                state = State.ALLOWED
            elif msg.same_topic(EXIT_ACK_TOPIC.topic):
                state = State.DRIVING
                should_recalibrate = True
                robot.start_recalibrate()

        # Updates time info
        last_time = current_time
        current_time = time.time()
        delta_time = current_time - last_time
        no_position_log_since += delta_time

        # Log position if needed (e.g. last position sent time > `MIN_POSITION_LOG_TIME`)
        if no_position_log_since >= MIN_POSITION_LOG_TIME:
            no_position_log_since = 0
            messenger.send_msg(POSITION_TOPIC, "{},{},{},{}".format(
                robot_id, position.traveled_dist, robot.get_steering(), position.road.get_num()))

        # If enter in a critical road, set the new state
        if position.is_critical(CRITICAL_ZONE_SIZE) and state == State.DRIVING:
            state = State.ENTER_CRITICAL

        # When the cross road is crossed with a given safe distance, we update the state
        if next_road == position.road and position.traveled_dist >= CROSS_DST and state == State.ALLOWED:
            state = State.EXITING

        # Send enter critical topic until the server does not acknowledge the action
        if state == State.ENTER_CRITICAL:
            messenger.send_msg(ENTER_CRITICAL_TOPIC, "{},{},{}".format(
                robot_id, position.road.get_num(), position.traveled_dist
            ))
            next_road = position.road.get_next()

        # Send exit critical topic until the server does not acknowledge the action
        if state == State.EXITING:
            messenger.send_msg(EXIT_CRITICAL_TOPIC, "{}".format(robot_id))

        # Drives the robot
        should_recalibrate = robot.drive(delta_time, force=state == State.ALLOWED, recalibrate=should_recalibrate)

        # Get back the robot position
        position = robot.get_position()

        # compute the time to wait by taking time went during previous steps
        d = time.time() - current_time

        # Forces sleep if delta time is to short
        if d < MIN_DELTA_TIME:
            sleep(MIN_DELTA_TIME - d)

    # Stop the messaging thread
    messenger.run = 0
    sleep(5)


if __name__ == '__main__':
    messenger.connect()
    print("connected")

    # Start the main thread
    _thread.start_new_thread(main, ())

    while messenger.run != 0:
        sleep(0.1)

    messenger.disconnect_lwt()
