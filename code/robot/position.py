from ucollections import deque
from math import *
import time
from direction import *


class Position:
    """
    This enumeration gives road position according to the circuit in 8
    """

    __dirs = {
        "-1": "unknown",
        "1": "intersection",
        "2": "first right turn",
        "3": "second right turn",
        "4": "straight right turn",
        "5": "first left turn",
        "6": "second left turn",
        "7": "straight left turn",
    }

    UNKNOWN = -1
    INTERSECTION = 1
    FIRST_RIGHT_TURN = 2
    SECOND_RIGHT_TURN = 3
    STRAIGHT_RIGHT_TURN = 4
    FIRST_LEFT_TURN = 5
    SECOND_LEFT_TURN = 6
    STRAIGHT_LEFT_TURN = 7

    @staticmethod
    def to_str(position: int):
        """
        Utility method for printing a position

        :param position: the position to translate into string

        :return the string representation of position
        """
        return Position.__dirs.get(str(position), "unknown")


class PositionEstimator:
    """
    This class tries to estimate the robot position in the circuit according to the direction sequence
    """

    def __init__(self, lookup_period_ms=500):
        """
        Initiates a PositionEstimator

        :param lookup_period_ms: defines in ms the time for which a direction is considered to be alive and valid
        """
        self._last_position = Position.UNKNOWN
        self._last_direction = Direction.UNKNOWN
        self._new_position = Position.UNKNOWN
        self._new_direction = Direction.UNKNOWN
        self._cumulated_time = 0
        self._lookup_period = lookup_period_ms

    def estimate(self, direction, delta_time):
        """
        Estimates the position of the robot using a kind of state machine

        :param direction: the estimated robot direction
        :param delta_time: the time elapsed between now and the last call to this method
        """

        if direction == Direction.UNKNOWN:
            return Position.UNKNOWN

        if self._new_direction != direction and self._last_direction != direction:
            if direction == Direction.TURNING_RIGHT:
                if self._last_position == Position.STRAIGHT_RIGHT_TURN:
                    self._new_position = Position.SECOND_RIGHT_TURN
                else:
                    self._new_position = Position.FIRST_RIGHT_TURN
            elif direction == Direction.TURNING_LEFT:
                if self._last_position == Position.STRAIGHT_LEFT_TURN:
                    self._new_position = Position.SECOND_LEFT_TURN
                else:
                    self._new_position = Position.FIRST_LEFT_TURN
            else:
                if self._last_position == Position.FIRST_LEFT_TURN:
                    self._new_position = Position.STRAIGHT_LEFT_TURN
                elif self._last_position == Position.FIRST_RIGHT_TURN:
                    self._new_position = Position.STRAIGHT_RIGHT_TURN
                else:
                    self._new_position = Position.INTERSECTION

            self._new_direction = direction
            self._cumulated_time = 0
            return self._last_position

        self._cumulated_time += delta_time
        if self._cumulated_time > self._lookup_period and self._new_direction == direction :
            self._last_position = self._new_position
            self._last_direction = self._new_direction
            return self._last_position

        return self._last_position
