
class Topic:
    """
    This class holds data about a MQTT topic, e.g. topic string and qos
    """

    def __init__(self, topic: str, qos: int):
        """
        Creates an instance of Topic

        :param topic: the topic string
        :param qos: the MQTT qos
        """
        self.topic = topic
        self.qos = qos


# All topics from EV3 to Server
ENTER_CRITICAL_TOPIC = Topic("TR54/g2/ev3/enter_critical", 1)
EXIT_CRITICAL_TOPIC = Topic("TR54/g2/ev3/exit_critical", 1)
POSITION_TOPIC = Topic("TR54/g2/ev3/position", 0)
REQUEST_ROBOT_ID_TOPIC = Topic("TR54/g2/ev3/get_id", 1)
RELEASE_ROBOT_ID_TOPIC = Topic("TR54/g2/ev3/release_id", 1)

# All topics from Server to EV3
ENTER_ACK_TOPIC = Topic("TR54/g2/server/{}/enter_ack", 1)
EXIT_ACK_TOPIC = Topic("TR54/g2/server/{}/exit_ack", 1)
ALLOWED_TOPIC = Topic("TR54/g2/server/{}/allowed", 1)
SEND_ROBOT_ID_TOPIC = Topic("TR54/g2/server/{}/id", 1)
