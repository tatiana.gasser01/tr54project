from road import *


class RobotDriver:
    """
    This class represent the state of a robot which is driving
    """

    def __init__(self, r_id, r_dist=0, r_road=RoadEnum.UNKNOWN):
        """
        Creates a new RobotDriver
        :param r_id: the ev3 id given by the server
        :param r_dist: the traveled distance in cm
        :param r_road: the road of the robot
        """
        self._id = r_id
        self._road_pos = RoadPosition(get_road(r_road), r_dist)
        self._allowed = False
        self._is_critical = False

    def update_position(self, r_road: RoadEnum, r_traveled: float):
        """
        Updates the position of the robot on the road
        :param r_road: the actual road
        :param r_traveled: the traveled distance on the road in cm
        :return:
        """
        self._road_pos = RoadPosition(get_road(r_road), r_traveled)

    def get_position(self):
        """
        Returns the robot position
        :return: the road position
        """
        return self._road_pos

    def get_id(self):
        """
        Returns the robot id
        :return: the robot id
        """
        return self._id

    def set_critical(self):
        """
        Sets the robot to be in critical zone
        :return: Nothing
        """
        self._is_critical = True

    def reset_critical(self):
        """
        Resets the critical flag
        :return: Nothing
        """
        self._is_critical = False

    def is_critical_set(self):
        """
        Returns the critical flag
        :return: the critical flag
        """
        return self._is_critical

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                getattr(other, '_id', None) == self._id)

    def __hash__(self):
        return hash(self._id)
