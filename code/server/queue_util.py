import time
import queue


def get_timed_interruptable(_q: queue.Queue, timeout):
    """
    Utility function to get a value in a queue with Ctrl-C enabled
    :param _q: a queue
    :param timeout: maximum time to wait
    :return: the popped value
    """
    stop_loop = time.monotonic() + timeout - 1
    while time.monotonic() < stop_loop:
        try:
            return _q.get(timeout=1)  # Allow check for Ctrl-C every second
        except queue.Empty:
            pass
    # Final wait for last fraction of a second
    return _q.get(timeout=max(0, stop_loop + 1 - time.monotonic()))
