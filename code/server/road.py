from enum import IntEnum
import sys


class RoadEnum(IntEnum):
    """
    An enumeration representing a road type
    """

    UNKNOWN = -1

    BOTTOM_RIGHT = 1
    BOTTOM = 2
    BOTTOM_LEFT = 3

    TOP_RIGHT = 4
    TOP = 5
    TOP_LEFT = 6


class Road:
    """
    This class holds data about the configuration of a road
    """

    def __init__(self, road_type: RoadEnum, next_road: RoadEnum, prev_road: RoadEnum,
                 road_length: float, is_critical=False):
        """
        Initiates the Road

        :param road_type: the type of the road, must be a value of RoadEnum
        :param next_road: the type of the road next to this, must be a value of RoadEnum
        :param prev_road: the type of the road preceding this one
        :param road_length: the type length of this road in cm
        :param is_critical: set to True if the road is connected to a cross road
        """

        self._type = road_type
        self._length = road_length
        self._is_critical = is_critical
        self._next_road = next_road
        self._prev_road = prev_road

    def get_next(self):
        """
        Returns the road next to this
        """
        return _ROADS[self._next_road]

    def get_previous(self):
        """
        Returns the road preceding this
        """
        return _ROADS[self._prev_road]

    def get_length(self):
        """
        Returns the road length
        """
        return self._length

    def get_num(self):
        """
        Returns the road type
        """
        return self._type

    def is_critical(self):
        """
        Returns True if the road is considered as critical e.g. connected to a cross road
        """
        return self._is_critical

    def remaining_dist(self, traveled_dist):
        """
        Computes the remaining distance between going to the next road

        :param traveled_dist: the traveled distance in cm
        """
        return self._length - traveled_dist


# defines the road graph
_ROADS = {
    RoadEnum.UNKNOWN: Road(
        RoadEnum.UNKNOWN,
        RoadEnum.UNKNOWN,
        RoadEnum.UNKNOWN,
        sys.float_info.max),

    RoadEnum.BOTTOM_RIGHT: Road(
        RoadEnum.BOTTOM_RIGHT,
        RoadEnum.TOP_LEFT,
        RoadEnum.BOTTOM,
        100, is_critical=True),

    RoadEnum.TOP_LEFT: Road(
        RoadEnum.TOP_LEFT,
        RoadEnum.TOP,
        RoadEnum.BOTTOM_RIGHT,
        90),

    RoadEnum.TOP: Road(
        RoadEnum.TOP,
        RoadEnum.TOP_RIGHT,
        RoadEnum.TOP_LEFT,
        102.96),

    RoadEnum.TOP_RIGHT: Road(
        RoadEnum.TOP_RIGHT,
        RoadEnum.BOTTOM_LEFT,
        RoadEnum.TOP,
        100, is_critical=True),

    RoadEnum.BOTTOM_LEFT: Road(
        RoadEnum.BOTTOM_LEFT,
        RoadEnum.BOTTOM,
        RoadEnum.TOP_RIGHT,
        90),

    RoadEnum.BOTTOM: Road(
        RoadEnum.BOTTOM,
        RoadEnum.BOTTOM_RIGHT,
        RoadEnum.BOTTOM_LEFT,
        102.96),
}


def get_road(road_num: RoadEnum):
    """
    Returns an instance of Road according to the given road type

    :param road_num: the road type, must be a value of RoadEnum
    """
    return _ROADS[road_num]


class RoadPosition:
    """
    This class holds and compute automatically the position of the robot on the road
    """

    def __init__(self, road: Road, traveled_dist: float):
        """
        Initiates a RoadPosition

        :param road: defines the starting road
        :param traveled_dist: defines the traveled distance in cm from the start of road
        """
        self.road = road
        self.traveled_dist = traveled_dist
        self.compute_new_position(0)

    def is_critical(self, critical_zone_length):
        """
        Returns True if the robot is a critical section

        :param critical_zone_length: gives in cm the size of the critical zone
        """
        if not self.road.is_critical():
            return False

        return True if self.road.remaining_dist(self.traveled_dist) <= critical_zone_length else False

    def remaining_dist(self):
        """
        Returns the remaining distance before a road transition
        """
        return self.road.remaining_dist(self.traveled_dist)

    def compute_new_position(self, traveled):
        """
         Computes the new position according to the traveled distance

         :param traveled: defines in centimetres the traveled distance between now and the last call to this method
         """
        while True:
            remaining = self.road.remaining_dist(self.traveled_dist + traveled)
            if remaining < 0:
                self.road = self.road.get_next()
                self.traveled_dist = -remaining
            else:
                self.traveled_dist += traveled
                break

    def compute_dist_to_cross_road(self, is_critical_set=True):
        """
        Computes the remaining distance before reaching the cross road stop line

        :param is_critical_set: set to True if the robot has informed the server of its critical state
        :return: a float giving in cm the remaining distance to the cross road
        """
        road = self.road
        s = self.road.remaining_dist(self.traveled_dist)

        if road.is_critical():
            return s

        if is_critical_set and road.get_previous().is_critical():
            return -self.traveled_dist

        road = road.get_next()

        while not road.is_critical():
            s += road.get_length()
            road = road.get_next()

        return s + road.get_length()

    def __str__(self):
        return "road: {}, traveled: {}".format(self.road.get_num(), self.traveled_dist)

