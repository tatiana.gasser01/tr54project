import paho.mqtt.client as mqtt
from queue import Queue

from queue_util import get_timed_interruptable
from topics import *
from scheduler import *
from road import *


class Message:
    """
    This class holds data for a MQTT message, e.g. topic and payload
    """

    def __init__(self, topic: str, payload: str):
        """
        Creates a Message

        :param topic: the MQTT topic
        :param payload: the MQTT payload
        """
        self.topic: str = topic
        self.payload: str = payload

    def same_topic(self, topic):
        """
        Checks if the given topic is the same of this

        :param topic: the topic to be compared with this.topic
        """
        return self.topic == topic


# defines the timeout in seconds to wait for a data
QUEUE_TIMEOUT = 60 * 60 * 24

# defines in cm the size of the zone in which the server will make the final scheduling decision
DECISION_ZONE = 30  # in cm

# defines the maximum length in cm between two robots to consider them as a convoy
CONVOY_INTER_SIZE = 60  # in cm, the maximum distance between two vehicles to consider them as a convoy

# topics to subscribe to
SUBSCRIBES = (ENTER_CRITICAL_TOPIC, EXIT_CRITICAL_TOPIC, POSITION_TOPIC,
              REQUEST_ROBOT_ID_TOPIC, RELEASE_ROBOT_ID_TOPIC)

# internal message queue to pipe messages from MQTT thread to the main thread
queue = Queue()


def send_topic(mqtt_client: mqtt.Client, topic: Topic, *args, payload=None):
    """
    Sends a MQTT message
    :param mqtt_client: the MQTT client to be used
    :param topic: the MQTT topic
    :param args: facultative args to apply to topic format function
    :param payload: the MQTT payload of the message
    :return: Nothing
    """
    print('Send message {} with payload {}'.format(topic.topic.format(*args), payload))
    mqtt_client.publish(topic.topic.format(*args), qos=topic.qos, payload=payload)


def check_next_allowed(mqtt_client: mqtt.Client, scheduler: Scheduler, force):
    """
    Schedules and send MQTT allowance message if needed

    :param mqtt_client: the MQTT client
    :param scheduler: a scheduler instance
    :param force: if set to True send the allowance message even if the later does not have changed
    :return: Nothing
    """
    robots = scheduler.schedule(none_if_same=not force)
    if robots is not None:
        for r in robots:
            send_topic(mqtt_client, ALLOWED_TOPIC, r.get_id())


def update_position(scheduler: Scheduler, driver: RobotDriver, road_type: RoadEnum, dist: float):
    """
    Updates the position of a robot
    :param scheduler: used scheduler instance
    :param driver: the robot driver
    :param road_type: the road of the robot
    :param dist: the traveled distance in cm
    :return:
    """
    old_road = driver.get_position().road.get_num()
    driver.update_position(road_type, dist)
    scheduler.apply_road_change(driver, old_road)


def on_message(_, __, message):
    """
    Stores received MQTT messages into the queue
    :param _: ignored
    :param __: ignored
    :param message: the received MQTT message
    :return:
    """
    msg = Message(message.topic, message.payload.decode())
    queue.put(msg)


def main(args):
    """
    Main loop in charge of listening MQTT messages and scheduling the cross road

    :param args: defines the parameters parsed from command line input
    :return:
    """
    max_robot_id = int(args['max_id'])
    server_id = args['id']

    broker_ip = str(args['broker'])
    broker_port = args['port']

    available_ids = [True] * (max_robot_id + 1)
    robots = [RobotDriver(i) for i in range(max_robot_id + 1)]
    # scheduler = SimpleFIFOScheduler()
    # scheduler = NearestScheduler(DECISION_ZONE)
    scheduler = ConvoyScheduler(DECISION_ZONE, CONVOY_INTER_SIZE)
    client = mqtt.Client(server_id)

    if args['user'] and args['password']:
        client.username_pw_set(args['user'], args['password'])

    try:
        print('Connect to {} with port {}'.format(broker_ip, broker_port))
        client.connect(broker_ip, broker_port)
        client.loop_start()

        client.on_message = on_message

        for s in SUBSCRIBES:
            print('Subscribe to {} with qos {}'.format(s.topic, s.qos))
            client.subscribe(s.topic, qos=s.qos)

        while True:
            msg: Message = get_timed_interruptable(queue, QUEUE_TIMEOUT)

            if msg.topic == ENTER_CRITICAL_TOPIC.topic:
                print('Received enter critical with payload {}'.format(msg.payload))
                d = msg.payload.split(',')
                r_id = int(d[0])

                if 0 <= r_id <= max_robot_id:
                    r = robots[r_id]
                    send_topic(client, ENTER_ACK_TOPIC, r.get_id())

                    if len(d) >= 3:
                        update_position(scheduler, r, RoadEnum(int(d[1])), float(d[2]))

                    r.set_critical()
                    scheduler.enter_critical(r)
                    check_next_allowed(client, scheduler, True)

            elif msg.topic == EXIT_CRITICAL_TOPIC.topic:
                print('Received exit critical with payload {}'.format(msg.payload))
                d = msg.payload.split(',')
                r_id = int(d[0])

                if 0 <= r_id <= max_robot_id:
                    r = robots[int(d[0])]

                    send_topic(client, EXIT_ACK_TOPIC, r.get_id())

                    r.reset_critical()
                    scheduler.exit_critical(r)
                    check_next_allowed(client, scheduler, False)

            elif msg.topic == POSITION_TOPIC.topic:
                d = msg.payload.split(',')
                r_id = int(d[0])

                if 0 <= r_id <= max_robot_id:
                    d = msg.payload.split(',')
                    r = robots[int(d[0])]
                    update_position(scheduler, r, RoadEnum(int(d[3])), float(d[1]))
                    check_next_allowed(client, scheduler, False)

            elif msg.topic == REQUEST_ROBOT_ID_TOPIC.topic:
                print('Received id request with payload {}'.format(msg.payload))
                d = msg.payload.split(',')
                r_id = next((i for i, j in enumerate(available_ids) if j), None)
                if r_id is not None:
                    available_ids[r_id] = False
                else:
                    r_id = -1
                send_topic(client, SEND_ROBOT_ID_TOPIC, d[0], payload=str(r_id))

            elif msg.topic == RELEASE_ROBOT_ID_TOPIC.topic:
                print('Received id release with payload {}'.format(msg.payload))

                d = msg.payload.split(',')
                r_id = int(d[0])

                if 0 <= r_id <= max_robot_id:
                    available_ids[r_id] = True

            queue.task_done()

    except (KeyboardInterrupt, SystemExit):
        print('Exiting...')
    finally:
        client.loop_stop()
        client.disconnect()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--broker', help='defines broker ip', default='localhost')
    parser.add_argument('-u', '--user', help='specify MQTT username', default=None)
    parser.add_argument('-P', '--password', help='specify MQTT password', default=None)
    parser.add_argument('--port', help='define the broker port', default=1883, type=int)
    parser.add_argument('--id', help='defines the server id', default='server-8c9926da')
    parser.add_argument('--max-id', dest='max_id', help='defines the max number of robots id', default=3)

    a = parser.parse_args()
    main(vars(a))
