from robot import RobotDriver
from abc import ABC, abstractmethod
from road import *


class Scheduler(ABC):
    """
    Defines an abstract Scheduler allowing the user to choose which robot should have the right of way
    """

    def __init__(self):
        """
        Creates a new Scheduler
        """
        self._waiting_robots = []
        self._per_road_robots = {}
        self._known_robots = {}
        self._crossing_robots = []
        self._new_crossing_robots = []

        for r in RoadEnum:
            self._per_road_robots[r] = []

    def schedule(self, none_if_same=False):
        """
        Processes the robot positions which are flag as critical to decide which one has the right of way

        :param none_if_same: if set to True, None will be returned if the decision has not changed since the previous one
        :return: an array of selected robots or None
        """
        if len(self._waiting_robots) == 0:
            return None

        self._new_crossing_robots = []
        has_changed = self._schedule()

        if has_changed:
            self._crossing_robots.extend(self._new_crossing_robots)

        if not none_if_same:
            return self._crossing_robots

        if has_changed:
            return self._new_crossing_robots

        return None

    @abstractmethod
    def _schedule(self):
        """
        Internal method, must be implemented by children classes
        :return: True if a new robot have the right of way
        """
        raise Exception("Not implemented")

    def apply_road_change(self, r: RobotDriver, old_road: RoadEnum):
        """
        Applies the road change of a given robot

        :param r: the robot driver
        :param old_road: the road of the robot preceding the new road
        :return: Nothing
        """
        if self._known_robots.get(r, False) and r.get_position().road.get_num() != old_road:
            self._per_road_robots[old_road].remove(r)
            self._per_road_robots[r.get_position().road.get_num()].append(r)

    def enter_critical(self, r: RobotDriver):
        """
        Informs the scheduler that a robot enters in critical zone and must be taken into account in scheduling phase

        :param r: the robot driver entering the critical area
        :return: Nothing
        """
        stored = self._known_robots.get(r, False)
        if not stored:
            self._waiting_robots.append(r)
            self._per_road_robots[r.get_position().road.get_num()].append(r)
            self._known_robots[r] = True

    def exit_critical(self, r: RobotDriver):
        """
        Remove the given robot from the critical zone and consequently from the scheduling decisions
        :param r: the robot driver exiting the critical area
        :return:
        """
        if self._known_robots.get(r, False):
            self._waiting_robots.remove(r)
            self._per_road_robots[r.get_position().road.get_num()].remove(r)

            self._known_robots[r] = False

        if r in self._crossing_robots:
            self._crossing_robots.remove(r)


class SimpleFIFOScheduler(Scheduler):
    """
    Defines a very simple scheduler based on the FIFO method. e.g. the first robot entering in the critical zone
    is allowed, others wait
    """

    def __init__(self):
        super().__init__()

    def _schedule(self):
        if len(self._crossing_robots) != 0:
            return False

        self._new_crossing_robots = [self._waiting_robots[0]]
        return True


class NearestScheduler(Scheduler):
    """
    Defines a scheduler in which the decision of the right of way is taken at the last moment, the elected robot is the
    nearest one
    """

    def __init__(self, decision_zone_length):
        """
        Creates a new instance of NearestScheduler
        :param decision_zone_length: defines in cm the length to be reached from the nearest robot and the cross road
                                     before making the scheduling decision
        """
        super().__init__()
        self._decision_zone_length = decision_zone_length

    @staticmethod
    def _get_firsts(robots):
        dist = [(0, 0.0)] * len(robots)
        for i, r in enumerate(robots):
            dist[i] = (i, r.get_position().compute_dist_to_cross_road(is_critical_set=r.is_critical_set()))

        dist.sort(key=lambda x: x[1])
        return dist

    def _get_first(self, robots):
        dist = self._get_firsts(robots)
        return (self._waiting_robots[dist[0][0]], dist[0][1]) if len(robots) else (None, 0)

    def _schedule(self):
        if len(self._crossing_robots) != 0:
            return False

        first, d = self._get_first(self._waiting_robots)
        if d <= self._decision_zone_length:
            self._new_crossing_robots.append(first)
            return True
        return False


class ConvoyScheduler(NearestScheduler):
    """
    Defines a scheduler in which the decision of the right of way is taken at the last moment, the elected robot is the
    nearest one (same as NearestScheduler) but this logic is pre-empted if a convoy can be created
    """

    def __init__(self, decision_zone_length, max_convoy_dist):
        """
        Creates a new instance of NearestScheduler
        :param decision_zone_length: defines in cm the length to be reached from the nearest robot and the cross road
                                     before making the scheduling decision
        :param max_convoy_dist: defines the maximum length in cm between two robots to consider them as a convoy
        """
        super().__init__(decision_zone_length)
        self._max_convoy_dist = max_convoy_dist
        self._selected_road = RoadEnum.UNKNOWN

    def _schedule(self):

        if len(self._crossing_robots) != 0:
            convoy_road = self._selected_road
            robots_on_road = self._per_road_robots[convoy_road].copy()
            # append robots of the next road in case of a little displacement
            # (e.g. robot failed to stop before the cross)
            # should never add anything
            robots_on_road.extend(self._per_road_robots[get_road(convoy_road).get_next().get_num()])

            # Similarly we append previous road
            robots_on_road.extend(self._per_road_robots[get_road(convoy_road).get_previous().get_num()])

            not_in_crossing = list(set(robots_on_road) - set(self._crossing_robots))
            last_in_crossing: RobotDriver = self._crossing_robots[-1]
            d_last_crossing = abs(last_in_crossing.get_position().compute_dist_to_cross_road(True))
            ordered = self._get_firsts(not_in_crossing)

            for e in ordered:
                if abs(e[1]) + d_last_crossing > self._max_convoy_dist:
                    break

                self._new_crossing_robots.append(not_in_crossing[e[0]])

            return len(self._new_crossing_robots) != 0

        res = super()._schedule()
        if res:
            self._selected_road = self._new_crossing_robots[0].get_position().road.get_num()

        return res
